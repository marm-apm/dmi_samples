package de.oth.amp.dmn;

import java.io.InputStream;
import org.assertj.core.data.MapEntry;
import org.camunda.bpm.dmn.engine.DmnDecision;
import org.camunda.bpm.dmn.engine.DmnDecisionResult;
import org.camunda.bpm.dmn.engine.DmnDecisionTableResult;
import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.dmn.engine.test.DmnEngineRule;
import org.camunda.bpm.engine.variable.VariableMap;
import org.junit.Rule;
import org.junit.Test;

import static de.oth.amp.dmn.test.DmnEngineTestAssertions.assertThat;
import static org.camunda.bpm.engine.variable.Variables.*;
import static org.junit.Assert.*;

public class DecisionExamplesTest {

    @Rule
    public DmnEngineRule dmnEngineRule = new DmnEngineRule();

    @Test
    public void bmiCalculation() {
        DmnDecisionResult result = evaluateDecision(
                "/BMI.dmn",
                "bmi",
                createVariables().
                        putValue("weight", 55.0).
                        putValue("height", 1.75));

        System.out.println("result = " + result);

        double bmi = result.getSingleResult().getEntry("bmi");

        assertEquals(17.9, bmi, 0.1);
    }

    @Test
    public void bmiClassification() {
        DmnDecisionTableResult result = evaluateDecisionTable(
                "/BMI.dmn",
                "classification",
                createVariables().
                        putValue("weight", 55.0).
                        putValue("height", 1.75));

        System.out.println("result = " + result);

        assertThat(result).hasSingleResult().contains(
                MapEntry.entry("cat1", "leichtes Untergewicht"),
                MapEntry.entry("cat2", "Untergewicht"));
    }

    @Test
    public void bmiClassification_TableOnly() {
        DmnDecisionTableResult result = evaluateDecisionTable(
                "/BMI_table_only.dmn",
                "classificationTableOnly",
                createVariables().
                        putValue("weight", 55.0).
                        putValue("height", 1.75));

        System.out.println("result = " + result);

        assertThat(result).hasSingleResult().contains(
                MapEntry.entry("cat1", "leichtes Untergewicht"),
                MapEntry.entry("cat2", "Untergewicht"));
    }

    @Test
    public void countAndClassify() {
        DmnDecisionTableResult result = evaluateDecisionTable(
                "/count_and_classify.dmn",
                "classify",
                createVariables().
                        putValue("a", 4).
                        putValue("b", 20).
                        putValue("c", 6));

        System.out.println("result = " + result);

        assertThat(result).hasSingleResult().contains(
                MapEntry.entry("classification", "Kategorie A"));

    }

    @Test
    public void outputExpression() {
        DmnDecisionTableResult result = evaluateDecisionTable(
                "/action_table.dmn",
                "test_out_exp",
                createVariables().
                        putValue("a", 4).
                        putValue("b", 20).
                        putValue("c", 6).
                        putValue("bean", new TestBean(2)));

        System.out.println("result = " + result);

        assertThat(result).hasSingleResult().contains(
                MapEntry.entry("result", 120));

    }
    
    @Test
    public void outputExpression_beanCall() {
        DmnDecisionTableResult result = evaluateDecisionTable(
                "/action_table.dmn",
                "test_out_exp",
                createVariables().
                        putValue("a", 5).
                        putValue("b", 20).
                        putValue("c", 6).
                        putValue("bean", new TestBean(2)));

        System.out.println("result = " + result);

        assertThat(result.<Integer>getSingleEntry()).isEqualTo(62);

    }

    @Test
    public void actionTable_valCalc() {
        DmnDecisionTableResult result = evaluateDecisionTable(
                "/action_table.dmn",
                "decideValCalc",
                createVariables().
                        putValue("a", 4).
                        putValue("b", 6).
                        putValue("c", 6));

        System.out.println("result = " + result);

        assertThat(result).hasSingleResult().contains(
                MapEntry.entry("valCalc", 5));

    }
    
    @Test
    public void actionTable_calc() {
        DmnDecisionResult result = evaluateDecision(
                "/action_table.dmn",
                "calculate",
                createVariables().
                        putValue("a", 4).
                        putValue("b", 6).
                        putValue("c", 4));

        System.out.println("result = " + result);

        assertThat(result.<Double>getSingleEntry()).isEqualTo(1.25);

    }

    private DmnDecisionTableResult evaluateDecisionTable(String resource, String decisionKey, VariableMap variables) {
        DmnEngine dmnEngine = dmnEngineRule.getDmnEngine();
        InputStream inputStream = getClass().getResourceAsStream(resource);
        DmnDecision decision = dmnEngine.parseDecision(decisionKey, inputStream);
        return dmnEngine.evaluateDecisionTable(decision, variables);
    }

    private DmnDecisionResult evaluateDecision(String resource, String decisionKey, VariableMap variables) {
        DmnEngine dmnEngine = dmnEngineRule.getDmnEngine();
        InputStream inputStream = getClass().getResourceAsStream(resource);
        DmnDecision decision = dmnEngine.parseDecision(decisionKey, inputStream);
        return dmnEngine.evaluateDecision(decision, variables);
    }

}
